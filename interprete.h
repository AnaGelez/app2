#ifndef INTERPRETE_H
#define INTERPRETE_H


int execute(elts_pile elt, pile_ent_inst *p);

void push_int(pile_ent_inst *p, int val);

void push_instruc(pile_ent_inst *p, cellule_t * instruc, int taille2);
	
elts_pile pop(pile_ent_inst *p);

int interprete (sequence_t* seq, bool debug);

#endif
