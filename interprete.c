#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#ifdef NCURSES
#include <ncurses.h>
#endif
#include "listes.h"
#include "curiosity.h"


/*
 *  Auteur(s) :
 *  Date :
 *  Suivi des Modifications :
 *
 */





//pour ajouter un nombre à la pile
void push_int(pile_ent_inst *p, int val){
	p->pile[p->taille].est_instruction = false;
	p->pile[p->taille].nombre = val;
	p->taille++;
}

//ajouter une série d'instructions à la pile
void push_instruc(pile_ent_inst *p, sequence_t *instruc){
	p->pile[p->taille].est_instruction = true;
	p->pile[p->taille].instruction = instruc;
	p->taille++;
}


void afficher_pile(pile_ent_inst *p){
	printf("Pile :\n");
        for(int i = 0; i < p->taille; i++){
        	if(p->pile[i].est_instruction){
			cellule_t * cellnow = p->pile[i].instruction->tete;
            if (cellnow != NULL){
        			afficher(p->pile[i].instruction);
            }
        	}else{
        		printf("%d", p->pile[i].nombre);
        	}
        	printf("\n");
        }
printf("\n");
}
	
void append(sequence_t *seq, char c){
  cellule_t * cellnow;//fonction qui malloc une cellule contenant le caractere C et qui l'ajoute en fin de la sequence SEQ
	cellule_t * newcell = malloc(sizeof(cellule_t));
	printf("carac = %c\n",c);
  newcell->command=c;
 
  if(seq->tete==NULL){
    seq->tete=newcell;
     newcell->suivant = NULL;
  }else{
    cellnow = seq->tete;
    while (cellnow->suivant!=NULL){
      cellnow = cellnow->suivant;
    }
    cellnow->suivant = newcell;
     newcell->suivant = NULL;
  }
  // afficher_pile(&p);
}



//pour pop le dernier élément d'une pile, soit un nombre soit un instruction
elts_pile pop(pile_ent_inst *p){
	p->taille--;
	return p->pile[p->taille];
}




void stop (void)
{
    char enter = '\0';
    printf ("Appuyer sur entrée pour continuer...\n");
    while (enter != '\r' && enter != '\n') { enter = getchar(); }
}







	





int interprete (sequence_t* seq, bool debug)
{
    // Version temporaire a remplacer par une lecture des commandes dans la
    // liste chainee et leur interpretation.

    char commande;



    debug = true; /* À enlever par la suite et utiliser "-d" sur la ligne de commandes */

    printf ("Programme:");
    afficher(seq);
    printf ("\n");
    if (debug) stop();

    // À partir d'ici, beaucoup de choses à modifier dans la suite.

    
    int ret;         //utilisée pour les valeurs de retour
	
    pile_ent_inst p;
	p.taille = 0;
	int a,b;
cellule_t * cellnow = seq->tete;
	int niveau = 0;
  sequence_t *instruct;

	int taille_instruc = 0;
	elts_pile faux;
	elts_pile vrai;
	elts_pile exec;
	int condition;
   	while (cellnow != NULL) { 
   		printf("niveau = %d\ncommande = '%c' (%d)\n", niveau, cellnow->command, cellnow->command);
   		
    commande = cellnow->command ;
		if (niveau > 0){
			switch (commande){
				case '}':
					niveau--;
					if (niveau == 0){
						push_instruc(&p, instruct);
						// cellnow = seq->tete;
						taille_instruc = 0;
					}else{
						append(instruct, commande);
					}
					break;
				case '{':
					niveau++;
					append(instruct, commande);
					break;
				default:
					append(instruct, commande);
			}
		}else{	
		    switch (commande) {
		        case 'A':
		            ret = avance();
		            if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
		            if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
		            break; /* à ne jamais oublier !!! */
				case '\n':
					break;
				case ' ':
					break;
				case 'D':
					droite();
					break;
				case 'G':
					gauche();
					break;
				
				case '0':
					push_int(&p, 0);
					break;
				case '1':
					push_int(&p, 1);
					break;
				case '2':
					push_int(&p, 2);
					break;
				case '3':
					push_int(&p, 3);
					break;
				case '4':
					push_int(&p, 4);
					break;
				case '5':
					push_int(&p, 5);
					break;
				case '6':
					push_int(&p, 6);
					break;
				case '7':
					push_int(&p, 7);
					break;
				case '8':
					push_int(&p, 8);
					break;
				case '9':
					push_int(&p, 9);
					break;
					// pour addition, mult etc, est ce qu'on doit s'ssurer que les deux derniers éléments de la pile sont bien des entiers ?
				case '+':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, a+b);
					break;
				case '*':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, a*b);
					break;
				case '-':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, b-a);
					break;
				case 'P':
					a = pop(&p).nombre;
					pose(a);
					break;
				case 'M':
					a = pop(&p).nombre;
					int b = mesure(a);
					push_int(&p, b);
					break;

				case '{':
					niveau = 1;
                    instruct = malloc(sizeof(sequence_t));
					// debut_instruc = cellnow->suivant;
					break;
					
				case '?':
					faux = pop(&p);
					
					afficher_pile(&p);
					vrai = pop(&p);
					
					afficher_pile(&p);
					condition = pop(&p).nombre;
					
					afficher_pile(&p);
					if(condition == 0){
						ret = interprete(faux.instruction, debug);
						if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
		            	if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
		            	
		            	afficher_pile(&p);
					}else{
						ret = interprete(vrai.instruction, debug);
						if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
		            	if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
		            
		            	afficher_pile(&p);
					}
					break;
				case '!':
					exec = pop(&p);
					ret = interprete(exec.instruction, debug);
					if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
		            if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
					break;
	
				/*case 'B':
					n = pop(&p);
					exec = pop(&p);
					if (n.nombre > 0){
						newcell = malloc(sizeof(cellule_t));
						newcell->command = 'B';
						p.pile[p.taille].instruction = newcell;
						p.pile[p.taille].est_instruction = true;
						p.pile[p.taille].nombre = 1;
						p.taille++;
					}
					break;*/
		        default:
		            eprintf("Caractère inconnu: '%c'\n", commande);
		    }
		}
        

		afficher_pile(&p);

		cellnow=cellnow->suivant;
		// commande = cellnow->command ;
        /* Affichage pour faciliter le debug */
        afficherCarte();
        printf ("Programme:");
        afficher(seq);
        printf ("\n");
        // if (debug) stop();
    }

    /* Si on sort de la boucle sans arriver sur la cible,
     * c'est raté :-( */

    return CIBLERATEE;
}
