#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#ifdef NCURSES
#include <ncurses.h>
#endif
#include "listes.h"
#include "curiosity.h"



/*
 *  Auteur(s) :
 *  Date :
 *  Suivi des Modifications :
 *
 */

void stop (void)
{
    char enter = '\0';
    printf ("Appuyer sur entrée pour continuer...\n");
    while (enter != '\r' && enter != '\n') { enter = getchar(); }
}

//Execute seq (limité au limit-ième caractère, -1 pour infini), en utilisant la pile s
int execute(sequence_t *seq, int limit, stack *s, bool debug) {
 
    //Variables utilisés dans le switch
    int a, b;
    stack_elem if_inst, else_inst;
    int condition;

    //Le niveau d'accolade dans lequel on se trouve :
    // {AA{DGA}GA}
    //^ ^  ^   ^  ^
    //0 1  2   1  0
    int level = 0;
    //Le début de l'instruction (d'une structure conditionelle) et sa taille
    cellule_t *instruction_start = NULL;
    int instruction_size = 0;
    sequence_t seq_inst;

    //Commande de la séquence à effectuer
    char commande = seq->tete->command; 
    //utilisée pour les valeurs de retour
    int ret;         
    
    while (seq->tete != NULL && limit != 0) { //à modifier: condition de boucle
        printStack(s);
        //Si on est dans une instruction
        if (level > 0) {
            switch(commande) {

                case '}':
                    level--;

                    //Si on ferme la dernière accolade d'instruction
                    if (level == 0) {
                        //On crée une instruction qu'on empile
                        stack_elem e = {
                            true,
                            0,
                            instruction_start,
                            instruction_size
                        };
                        push(e, s);
                        instruction_start = NULL;
                        instruction_size = 0;
                    }
                    else {
                        instruction_size++;
                    }

                    break;

                case '{':
                    level++;
                    instruction_size++;
                    break;

                default:
                    instruction_size++;
            }
        }
        else { //Dans tous les autres cas  
            switch (commande) {
                /* Ici on avance tout le temps, à compléter pour gérer d'autres commandes */

                case 'A':
                    ret = avance();
                    if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
                    if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
                    break;
                
                case 'G':
                    gauche();
                    break;

                case 'D':
                    droite();
                    break;

                case '+':
                    //On dépile les deux éléments au sommet de la pile et on empile leur somme
                    a = pop(s).number;
                    b = pop(s).number;
                    push_nb(b+a,s);
                    break;

                case '-':
                    //IDEM pour -
                    a = pop(s).number;
                    b = pop(s).number;
                    push_nb(b-a,s);
                    break;

                case '*':
                    //IDEM pour *
                    a = pop(s).number;
                    b = pop(s).number;
                    push_nb(b*a,s);
                    break;

                case 'P':
                    pose(pop(s).number);
                    break;
                
                case 'M':
                    a = mesure(pop(s).number);
                    push_nb(a,s);
                    break;

                case '{' : //On monte de niveau, on rentrera dans l'autre switch pour la prochaine commande
                    level = 1;
                    instruction_start = seq->tete->suivant;
                    break;
                
                case '?': //On récupère les instructions et la condition
                    else_inst = pop(s);
                    if_inst = pop(s);
                    condition = pop(s).number;
                    if (condition != 0) { //En fonction de la condition, on exécute l'instruction if ou else
                        seq_inst.tete = if_inst.instruction;
                        execute(&seq_inst, if_inst.instruction_size, s, debug);

                        // ****** Uniquement pour l'affichage **********
                        printf("TO EXECUTE (lim = %d)\n*****************\n",if_inst.instruction_size);
                        afficher(&seq_inst);
                        printf("*****************\n");
                        // *********************************************
                    }
                    else {
                        seq_inst.tete = else_inst.instruction;
                        execute(&seq_inst, else_inst.instruction_size, s, debug);
                    }
                    break;

                default:
                    //si on a un nombre entre 0 et 9
                    if (commande >= '0' && commande <='9') {
                        int n = commande - '0';
                        push_nb(n,s);
                    }
                    else
                        eprintf("Caractère inconnu: '%c'\n", commande);
            }
        }

        /* Affichage pour faciliter le debug */
        afficherCarte();
        printf ("Programme:");
        afficher(seq);
        printf ("\n");
        printStack(s);
        if (debug) stop();

        seq->tete = seq->tete->suivant;
        commande = seq->tete->command;
        if (limit > 0)
            limit--;
    }
    return -1;
}

//La fonction interprète, qui repose sur execute pour fonctionner
int interprete (sequence_t* seq, bool debug)
{
    // Version temporaire a remplacer par une lecture des commandes dans la
    // liste chainee et leur interpretation.

    stack *s = newStack();

    printf ("Programme:");
    afficher(seq);
    printf ("\n");
    if (debug) stop();

    int ret = execute(seq, -1, s, debug);
    if (ret != -1)
        return ret;

    /* Si on sort de la boucle sans arriver sur la cible,
     * c'est raté :-( */

    return CIBLERATEE;
}