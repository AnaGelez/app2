#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#ifdef NCURSES
#include <ncurses.h>
#endif
#include "listes.h"


/*
 *  Auteur(s) :
 *  Date :
 *  Suivi des Modifications :
 *
 */

bool silent_mode = false;


cellule_t* nouvelleCellule (void)
{
	
	cellule_t *newcell = malloc(sizeof(cellule_t));
    return newcell;
}


void ajout_en_queue(sequence_t *l, char n) {
    cellule_t* cellnow = l->tete;
    cellule_t* newcell = nouvelleCellule();
    newcell->command = n;
   
    if (cellnow == NULL){
        cellnow = newcell;
        l->tete = newcell;
        
        newcell->suivant = NULL;
        return;
    }
    while (cellnow->suivant != NULL){
        cellnow = cellnow->suivant;
    }
    cellnow->suivant = newcell;
    newcell->suivant = NULL;


return;}



void detruireCellule (cellule_t* cel)
{
    free(cel);
}



void conversion (char *texte, sequence_t *seq)
{
	seq->tete = NULL;
  for (int i = 0; texte[i] !='\0'; i++){
  	if(texte[i] != ' '){
  		ajout_en_queue(seq, texte[i]);
  		}
  	}
}



void afficher(sequence_t* seq)
{
    assert (seq); /* Le pointeur doit être valide */
    cellule_t* actcell;
	if(seq->tete != NULL){
    actcell = seq->tete;
    while(actcell != NULL){
    	printf("%c", actcell->command);
    	actcell = actcell->suivant;
	}
	}
}

