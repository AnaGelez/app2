#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#ifdef NCURSES
#include <ncurses.h>
#endif
#include "listes.h"
#include "curiosity.h"


/*
 *  Auteur(s) :
 *  Date :
 *  Suivi des Modifications :
 *
 */



void afficher_pile(pile_ent_inst *p){
	printf("Pile :\n");
        for(int i = 0; i < p->taille; i++){
        	if(p->pile[i].est_instruction){
			cellule_t * cellnow = p->pile[i].instruction;
        		for(int j=0; j <= p->pile[i].nombre -1 && cellnow->suivant != NULL ; j++){
				printf("%c", cellnow->command);
				cellnow = cellnow->suivant;
			}
        	}else{
        		printf("%d", p->pile[i].nombre);
        	}
        	printf("\n");
        }
}




void execute(elts_pile elt, cellule_t * cellnow){
	cellule_t * celllast = elt.instruction;
	if(elt.nombre != 0){
		for(int i = 0; i < elt.nombre; i++){ //si pb, c'est ça
			celllast = celllast->suivant;
		}
		celllast->suivant = cellnow->suivant;
		cellnow->suivant = elt.instruction;

	}
}
	


//pour ajouter un nombre à la pile
void push_int(pile_ent_inst *p, int val){
	p->pile[p->taille].est_instruction = false;
	p->pile[p->taille].nombre = val;
	p->taille++;
}

//ajouter une série d'instructions à la pile
void push_instruc(pile_ent_inst *p, cellule_t * instruc, int taille2){
	p->pile[p->taille].est_instruction = true;
	p->pile[p->taille].instruction = instruc;
	p->pile[p->taille].nombre = taille2;
	p->taille++;
}
	
	
//ANCIENNE FONCTION
/*char pop(pileEntiers *p){
	char c = p->pile[p->taille -1];
	p->pile[p->taille] = '\0';
	p->taille--;
	return c;
	}*/




//pour pop le dernier élément d'une pile, soit un nombre soit un instruction
elts_pile pop(pile_ent_inst *p){
	p->taille--;
	return p->pile[p->taille];
}




void stop (void)
{
    char enter = '\0';
    printf ("Appuyer sur entrée pour continuer...\n");
    while (enter != '\r' && enter != '\n') { enter = getchar(); }
}



int interprete (sequence_t* seq, bool debug)
{
    // Version temporaire a remplacer par une lecture des commandes dans la
    // liste chainee et leur interpretation.

    char commande;



    debug = true; /* À enlever par la suite et utiliser "-d" sur la ligne de commandes */

    printf ("Programme:");
    afficher(seq);
    printf ("\n");
    if (debug) stop();

    // À partir d'ici, beaucoup de choses à modifier dans la suite.

    cellule_t * cellnow = seq->tete;
    commande = cellnow->command ;
    int ret;         //utilisée pour les valeurs de retour
	pile_ent_inst p;
	p.taille = 0;
	int a,b;
	int niveau = 0;
	cellule_t * debut_instruc = NULL;
	int taille_instruc = 0;
	elts_pile faux;
	elts_pile vrai;
	elts_pile exec;
	int condition;
   	while (cellnow != NULL) { 
		if (niveau > 0){
			switch (commande){
				case '}':
					niveau--;
					if (niveau == 0){
						push_instruc(&p, debut_instruc, taille_instruc);
						debut_instruc = NULL;
						taille_instruc = 0;
					}else{
						taille_instruc++;
					}
					break;
				case '{':
					niveau++;
					taille_instruc++;
					break;
				default:
					taille_instruc++;
			}
		}else{	
		    switch (commande) {
		        case 'A':
		            ret = avance();
		            if (ret == VICTOIRE) return VICTOIRE; /* on a atteint la cible */
		            if (ret == RATE)     return RATE;     /* tombé dans l'eau ou sur un rocher */
		            break; /* à ne jamais oublier !!! */
				case 'D':
					droite();
					break;
				case 'G':
					gauche();
					break;
				
				case '0':
					push_int(&p, 0);
					break;
				case '1':
					push_int(&p, 1);
					break;
				case '2':
					push_int(&p, 2);
					break;
				case '3':
					push_int(&p, 3);
					break;
				case '4':
					push_int(&p, 4);
					break;
				case '5':
					push_int(&p, 5);
					break;
				case '6':
					push_int(&p, 6);
					break;
				case '7':
					push_int(&p, 7);
					break;
				case '8':
					push_int(&p, 8);
					break;
				case '9':
					push_int(&p, 9);
					break;
					// pour addition, mult etc, est ce qu'on doit s'ssurer que les deux derniers éléments de la pile sont bien des entiers ?
				case '+':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, a+b);
					break;
				case '*':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, a*b);
					break;
				case '-':
					a = pop(&p).nombre;
					b = pop(&p).nombre;
					push_int(&p, b-a);
					break;
				case 'P':
					a = pop(&p).nombre;
					pose(a);
					break;
				case 'M':
					a = pop(&p).nombre;
					int b = mesure(a);
					push_int(&p, b);
					break;

				case '{':
					niveau = 1;
					debut_instruc = cellnow->suivant;
					break;
					
				case '?':
					faux = pop(&p);
					vrai = pop(&p);
					condition = pop(&p).nombre;
					if(condition == 0){
						execute(faux, cellnow);
					}else{
						execute(vrai, cellnow);
					}
					break;
				case '!':
					exec = pop(&p);
					execute(exec, cellnow);
					break;
				case 'X':
					faux = pop(&p);
					vrai = pop(&p);
					if (vrai.est_instruction){
						push_instruc(&p, vrai.instruction, vrai.nombre);
					}else{
						push_int(&p, vrai.nombre);
					}if (faux.est_instruction){
						push_instruc(&p, faux.instruction, faux.nombre);
					}else{
						push_int(&p, faux.nombre);
					}
					break;
		        default:
		            eprintf("Caractère inconnu: '%c'\n", commande);
		    }
		}
        

		afficher_pile(&p);

		cellnow=cellnow->suivant;
		commande = cellnow->command ;
        /* Affichage pour faciliter le debug */
        afficherCarte();
        printf ("Programme:");
        afficher(seq);
        printf ("\n");
        if (debug) stop();
    }

    /* Si on sort de la boucle sans arriver sur la cible,
     * c'est raté :-( */

    return CIBLERATEE;
}
